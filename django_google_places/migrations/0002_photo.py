# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_google_places', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('reference', models.TextField(help_text=b'A token that can be used to query the Google Photos service.')),
                ('place', models.ForeignKey(related_name='photos', to='django_google_places.Place')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
