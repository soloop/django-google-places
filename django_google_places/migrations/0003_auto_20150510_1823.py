# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_google_places', '0002_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='place',
            name='utc_offset',
            field=models.IntegerField(help_text=b"The number of minutes this Place's current timezone is offset from UTC.", null=True, blank=True),
            preserve_default=True,
        ),
    ]
